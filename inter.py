import PySimpleGUI as sg
from main import resumo_livro

sg.theme('Dark')
row1 = [sg.Text('Nome do Livro'), sg.Input(key='livro')]
row2 = [sg.Button('Resumo')]
row3 = [sg.Text(key='resumo')]

layout = [row1, row2, row3]

window = sg.Window('Livros', layout)

window.read()

while True:
    event, values = window.read()
    if event == sg.WINDOW_CLOSED:
        break
    if event == 'Resumo':
        livro = values['livro']
        resumo = resumo_livro(livro)
        window['resumo'].update(resumo)
